<?php get_header(); 
?>

	<main role="main" aria-label="Content" >
	
		<!-- section -->
		<section class="article-wrap" id="main">

			<!-- <h1><?php /*_e( 'Latest Posts', 'html5blank' );*/ ?></h1> -->

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>
			
			<!--  * example for _e() * -->

			<?php _e('Hello ! ', 'sdadas'); ?>

		</section>
			
		<!-- /section -->
	</main>

<script>
	
</script>

<?php /*get_sidebar(); */ ?>

<?php get_footer(); ?>
