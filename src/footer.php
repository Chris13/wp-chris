			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="footer-wrap">
					<div class="logo">
						<div class="logo-image" style="background-image:url('<?php echo get_header_image(); ?>')">
							<a href="<?php echo home_url(); ?>">
								<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							</a>	
						</div>
					</div>
					
					<div class="links">
						<div class="pages">
							<?php footer_nav(); ?>
						</div>
						<p class="copyright">
							<!-- &copy; --> <?php /*echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. <?php _e('Powered by', 'html5blank');*/ ?>
							<!-- <a href="//wordpress.org">WordPress</a> &amp; <a href="//html5blank.com">HTML5 Blank</a>. -->
							&copy; 2017 Photography Blog. All rights reserved. <a href="javascript:void(0);">Terms &amp; Conditions.</a>
						</p>	
					</div>	
				</div>
				
				<!-- copyright -->
				
				<!-- /copyright -->

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
