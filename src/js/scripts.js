(function( root, $, undefined ) {
	"use strict";

	$(function () {
		// DOM ready, take it away
		$(".menubar").on("click", function(){
			// var activePoint = $(".nav").hasClass("nav-active");

			if($(".nav").hasClass("nav-active")) {
				$(".nav").removeClass("nav-active");
			} else {
				$(".nav").addClass("nav-active");
			}
		});

	});


} ( this, jQuery ));
		