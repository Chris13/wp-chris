<?php
/* 
	Template Name: Portfolio page
*/
?>

<!-- php code for portfolio -->
<?php 
	$cpt = array(
	    'post_type' => 'Portfolio'
	);

	$cptui = new WP_Query($cpt);
?>

<?php get_header(); ?>
	
	<main role="main" aria-label="main">
		<section class="article-wrap">
			
			<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php endwhile; endif; ?>

			

			<?php if( $cptui->have_posts() ) : while ($cptui->have_posts() ): $cptui->the_post(); ?>
				
				<article class="article-block">
					
					<div class="post-head">
						<div class="info">
							<h2 class="title">
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>
								</a>
							</h2>

							<p class="meta-info">
								<?php _e( 'by', 'html5blank' ); ?> 
								<?php the_author_posts_link(); ?> - 
								<a href="<?php the_author_meta('user_url'); ?>" target="_blank"><?php echo the_author_meta('user_url'); ?></a>
							</p>	
						</div>

						<div class="published">
							<div class="time-wrap"> 
								<time class="date-module" datetime="<?php the_time('Y-m-d'); ?> <?php the_time('H:i'); ?>">
									<div class="date">
										<?php the_time('j'); ?>
									</div>
									<div class="year">
										<?php the_time('F Y'); ?>
									</div>
								</time>
							</div>
						</div>
					</div>

					<div class="img-wrap">
						<?php the_post_thumbnail('500'); ?>
					</div>

					<p>
						<?php html5wp_excerpt('html5wp_index'); ?>
					</p>


				</article>
			<?php endwhile; endif; wp_reset_postdata(); ?>
	
	</section>
</main>

<?php get_footer(); ?>