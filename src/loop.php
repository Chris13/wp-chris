	<?php
	   /* $mypost = array( 'post_type' => 'movie_reviews', );
	    $loop = new WP_Query( $mypost );*/
	    $format = has_post_format('content', 'loop');
	?>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		
		<article class="article-block" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<?php endif; ?>
			
			<!--article info wrapper  -->

			<div class="post-head">
				<div class="published">
					
					<!-- module of date -->
						<div class="time-wrap"> 
							<time class="date-module" datetime="<?php the_time('Y-m-d'); ?> <?php the_time('H:i'); ?>">
								<div class="date">
									<?php the_time('j'); ?>
								</div>
								<div class="year">
									<?php the_time('F Y'); ?>
								</div>

								
							</time>
						</div>
					<!-- end of module of date -->
				
				</div>
				
				<div class="info">

					<!-- title of article -->
						<h2 class="title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							
							<!-- ***other usage:***-->
							<!-- <?php //the_title('<span>', '</span>', TRUE); ?> -->

						</h2>
					<!-- end of title -->
					
					<!-- meta info -->
						<p class="meta-info">
							<?php  echo __( 'by', 'html5blank' ); ?> 
							<?php get_avatar(get_the_author_meta('ID'), 24); ?>
							<?php the_author_posts_link(); ?> - 
								<a href="<?php the_author_posts_link(); ?>" target="_blank">
									<?php the_author_meta('user_url'); ?>	
								</a>
						</p>
					<!-- end of meta info -->
				
				</div>
			</div>
			
			<!-- image wrap -->
				<div class="img-wrap">
					<?php the_post_thumbnail();/*the_post_thumbnail(array(120,120));*/ // Declare pixel size you need inside the array ?>
				</div>
			<!-- end of image wrap -->

			<!-- paragraph -->
				<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
				<!-- <?php /*edit_post_link('Editing', '<p>', '</p>');*/ ?> -->
				<!-- <?php /*edit_post_link();*/ ?>  -->
			<!-- end of paragraph -->
			
			<!-- comment -->
			<!-- <span class="comments"><?php /*if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' ));*/ ?></span> -->
			<!-- /post details -->

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>
		<?php get_template_part( 'content', 'none' ); ?>

		<!-- article -->
		<article>
			<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
		</article>
		<!-- /article -->

	<?php endif; ?>
	
