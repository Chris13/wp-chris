<?php get_header(); ?>

	<main role="main" aria-label="Content">
	<!-- section -->
	<section class="article-wrap">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<!-- article -->
		<article class="article-block" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php the_date(); ?>
			<div class="post-head">
				<div class="info">
					<h2 class="title">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</h2>

					<p class="meta-info">
						<?php _e( 'by', 'html5blank' ); ?> 
						<?php the_author_posts_link(); ?> - 
						<a href="<?php the_author_meta('user_url'); ?>" target="_blank"><?php the_author_meta('user_url'); ?></a>
					</p>
				</div>
				<div class="published">
					<div class="time-wrap">
						<time class="date-module" datetime="<?php the_time('Y-m-d'); ?> <?php the_time('H:i'); ?>">
							<div class="date">
								<?php the_time('j'); ?>
							</div>
							<div class="year">
								<?php the_time('F Y'); ?>
							</div>
						</time>
					</div>
				</div>
			</div>

			<div class="img-wrap">
				<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
				
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail(); // Fullsize image for the single post ?>
				</a>

				<?php endif; ?>
			</div>

			<!-- /post title -->

			<!-- post details -->
			<!-- <?php /*if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' ));*/ ?></span> -->
			<!-- /post details -->

			<?php the_content(); // Dynamic Content ?>

			<?php //*the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end */?>

			<p><?php /*_e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas */?></p>

			<?php /* edit_post_link(); // Always handy to have Edit Post Links available */?>

			<?php /*comments_template();*/ ?>

			<!-- previous & next post -->
			<p>
				<?php previous_post_link('%link', 'Previous'); ?>
			</p>
			<p>	
				<?php next_post_link('%link', 'Next'); ?>
			</p>

			
			<!-- end of previous & next post -->
		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

<?php /*get_sidebar();*/ ?>

<?php get_footer(); ?>
