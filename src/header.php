<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>"></script>
		<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body >

		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<header class="header clear" role="banner">
				
					<!-- logo -->
					<!-- <div class="logo">

						Photography Blog --> <!-- suggest to use logo theme support -->
						<!-- <div class="logo-image" style="background-image:url('<?php /*echo get_header_image();*/ ?>')"> -->
							<!-- <a href="<?php /*echo home_url(); */?>"> -->
								<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<!-- </a>	 -->
						<!-- </div> -->
					<!-- </div> -->
					<!-- /logo -->
					
					<div class="searchbar">
						<i class="fa fa-search" aria-hidden="true"></i><!-- suggest to use background img as icon instead of using 'i' tag -->
					</div>

					<div class="menubar">
					
						<i class="fa fa-bars" aria-hidden="true"></i><!-- suggest to use background img as icon instead of using 'i' tag -->
					</div>
					<!-- nav -->
					<nav class="nav" role="navigation">
						<?php echo html5blank_nav(); ?>
					</nav>
					<!-- /nav -->

			</header>
			<!-- /header -->

			<div class="header-banner">
				<div class="web-logo" style="background-image:url('<?php echo get_header_image(); ?>')">
					<a href="<?php echo home_url(); ?>">
					</a>
				</div>
			</div>


